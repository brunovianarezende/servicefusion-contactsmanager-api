from setuptools import setup, find_packages

setup(
    name='servicefusion-contactsmanager-api',
    packages=find_packages(exclude=['tests']),
)
